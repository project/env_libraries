<?php

namespace Drupal\env_libraries\Twig;

use Drupal\env_libraries\Services\LibraryManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension for conditional env path.
 */
class EnvLibrariesTwigExtension extends AbstractExtension {

  /**
   * The env_libraries.library_manager service.
   *
   * @var \Drupal\env_libraries\Services\LibraryManager
   */
  protected LibraryManager $libraryManager;

  /**
   * Constructs a new EnvLibrariesTwigExtension object.
   *
   * @param \Drupal\env_libraries\Services\LibraryManager $library_manager
   *   The library manager.
   */
  public function __construct(LibraryManager $library_manager) {
    $this->libraryManager = $library_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('envPath', [$this, 'envPath']),
    ];
  }

  /**
   * Return env path.
   *
   * @param string $path
   *   The input path conditional.
   *
   * @return string
   *   The output path.
   */
  public function envPath(string $path) {
    return $this->libraryManager->getEnvPath($path);
  }

}
